---
title: "À propos"
date: 2018-12-14T16:01:30+01:00
draft: false
---


### Organisation

Trois commissions organisent les actions du Collectif:

 * Commission politique: représentation des intérêts des membres de l'association et des jeune(s) chercheurs(es) en général;
 * Commission scientifique: organisation de journée d'étude, séminaires, café-débats et autres activités scientifiques, préparation de soutenance blanche, etc;
 * Commission culturelle: organisation d'événements d'intégration des membres de l'association, d'événements culturels;
 * Commission revue : organisation et gestion de la [revue du collectif 
Don & 
Réciprocité](https://merrt.frama.io/collectif-marcel-mauss/revue-don-réciprocité/);



### Histoire du Collectif



### Pourquoi "Marcel Mauss" ?


