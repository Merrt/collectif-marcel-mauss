---
title: "Présentation de la revue Don & Réciprocité"
date: 2018-12-14T15:35:00+01:00
draft: false
---

![logo de la 
revue](https://framapic.org/HySq7Z3fk5P7/HI3mXfd1tvwl.jpg)


##### Qu'est-ce que la revue ?

La revue est issue d’un projet : celui de permettre aux doctorant.e.s et aux masterant.e.s en sciences sociales de pratiquer l’exercice de la publication dans une revue qui leur est réservée. La publication étant un exercice attendu, voire obligatoire lors des années de thèse, la revue permet aux étudiant.e.s de se « faire la main » et offre des opportunités de publication qui ne sont pas discriminantes ni le statut du chercheur, ni sur l’objet de sa recherche, ni sur la discipline appliquée. 

##### Qui est derrière la revue ?

Ce sont des doctorant.e.s rassemblés par le Collectif Marcel Mauss (collectif de doctorants en sciences sociales) qui sont à l’origine du projet. Plus précisément, une commission au sein du collectif (la « commission revue ») est en charge du projet.

##### Qui peut rédiger un document pour la revue ?

Cette revue est ouverte à tous les doctorant.e.s et masterant.e.s en sciences sociales en France et à l’étranger. L’objectif est de se préparer à l’exercice de la publication dans le respect de l’éthique et d’une scientificité rigoureuse.

##### Comment fonctionne la revue ?

La revue fonctionne en trois publications par année universitaire. Chaque publication se divise en quatre axes : deux axes thématiques changeant à chaque publication, un axe de publication libre et un axe de comptes-rendus et récensions. Elle est supportée numériquement par le blog du Collectif Marcel Mauss.

