---
title: "Publier dans la revue Don & Réciprocité"
date: 2018-12-14T15:30:00+01:00
draft: false
---

![logo de la 
revue](https://framapic.org/HySq7Z3fk5P7/HI3mXfd1tvwl.jpg)

Les textes proposés doivent êtres des documents originaux. Les articles comme les comptes-rendus doivent faire preuve d’impartialité, d’un traitement éthique des sujets abordés et d’une scientificité rigoureuse.

 * Les articles ne doivent pas dépasser les 40 000/60 000 signes (avec espaces) et les comptes-rendus ne doivent pas excéder les 12 000 signes (avec espaces).

 * Les articles seront accompagnés d’un résumé d’une dizaine de lignes.

 * Les noms, coordonnées, le domaine de recherche et le rattachement institutionnel de l’auteur devront figurer à la fin du document. 

 * Les articles seront rendus par courriel à l’adresse suivante :  @gmail.com 

 * Les textes soumis à la revue seront lus par les membres du comité de lecture. Sur cette base, le comité engage une discussion approfondie sur le texte afin d’arriver à une décision qui sera transmise par la suite à l’auteur du texte. Si besoin est, le comité se réserve le droit de solliciter un avis extérieur. 

 * La revue publie des textes rédigés en français et en anglais. 

 * En contrepartie de la publication l’auteur s’engage à céder à titre exclusif les droits de propriété à destination de la revue pour le monde entier, en tous formats et en tous supports, en diffusion, en l’état, traduite, adaptée et y compris sous forme d’exploitation numérique. Toutefois, sous condition d’une demande préalable l’auteur peut publier son texte dans un livre ou tout autre document dont il est l’auteur ou auquel il contribue sous réserve de citer la source de la première publication. 

##### Normes d'écritures

 * Les articles sont de préférence attendus sous le format suivant : **TimesNR/Arial/Calibri ; Police 12 ; Interligne 1.5 ; Justifié**.

 * Le texte proposé se doit d’être avec le moins de fautes possibles mais errare humanum est !

 * L’auteur reste libre d’un **plan visible** ou non (deux niveaux de titres maximum).

 * Les **notes de bas de pages** sont acceptés ainsi que les **images, graphiques et tableaux** à la condition de ne pas en abuser. 

 * Les **références bibliographiques** sont attendues sous le format « Harvard ».

 * Pour les comptes-rendus il est demandé un usage modéré **des citations**.

 * Pour toutes autre question n’hésitez pas à nous contacter par courriel.

 * Les fichiers seront envoyés sous **format** .doc sous la **forme** suivante :
 	- Comptes-rendus : CR_Titre_Nom_Année.doc
	- Articles : A_Titre_Nom_Année.doc
