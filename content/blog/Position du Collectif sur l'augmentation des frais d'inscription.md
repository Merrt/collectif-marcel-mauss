---
title: "Position Du CMM sur l'augmentation des frais d'inscription"
date: 2018-12-14T15:33:16+01:00
draft: false
---


*Le mercredi 10 décembre 2018, le Président de l'Université a demandé à l'ensemble des associations concernées de se positionner sur la mesure concernant la politique nationale d'ouverture
des universités aux étudiant(e)s - chercheur(e)s étrangers qui amène, paradoxalement, à l'augmentation des frais d'inscriptions de ces derniers. Ce communiqué se propose de répondre aux deux questions du Président:*




### Comment votre association voit-elle l'attractivité internationale de l'Université de Bordeaux dans ce nouveau contexte ?

 
Le Collectif Marcel Mauss ne peut que soutenir la dynamique d'ouverture internationale des universités. Pour nous, la mixité sociale et culturelle est bénéfique tant pour les étudiant·e·s, 
les doctorant·e·s, que pour la recherche de manière générale.  
Cependant, **nous regrettons les trop nombreuses situations de discrimination plus ou moins visibles** dont font l'expérience les 
doctorant·e·s de nationalité étrangère, que ce soit dans l'inégale répartition des postes de travail, des charges de cours, des emplois, ou encore en raison des critères différenciés 
d’attribution des aides sociales (non éligibilité à l'exonération des frais d'inscription pour les doctorant·e·s de nationalité étrangère inscrit·e·s en première année de doctorat). 
 
Pour nous, **l'attractivité internationale de l'Université de Bordeaux passe d'abord et avant tout par le fait de garantir à tous et toutes les mêmes conditions de travail et d'étude.** 
 
### Quelles sont vos propositions au niveau des règles d'exemption des frais d'inscription différenciés ? Quelles mesures particulières pourraient être mises en œuvre à l'intention des doctorants en provenance d’Afrique et des pays en développement ?
 
Le collectif Marcel Mauss s'associe à la position défendue par la Confédération des Jeunes Chercheurs(CJC) qui voit dans la mesure proposée par le gouvernement une course sans fin à la hausse 
des frais d’inscription derrière les modèles britanniques et américains, où la plupart des étudiant·e·s et doctorant·e·s s’endettent lourdement pour respectivement se former et travailler.  
Que cette hausse massive des frais d’inscription soit à ce stade limitée aux étranger·e·s non communautaires ne constitue qu’un élément aggravant. Les doctorant·e·s étranger·e·s sont déjà 
sélectionné·e·s par l’établissement d’enseignement supérieur dans lequel ils ou elles souhaitent s’inscrire, par l’employeur qui souhaite les recruter, par les services leur délivrant des 
visas... et payent un nombre conséquent de taxes et frais de dossiers pour venir travailler en France.  
L’argument de l’attractivité internationale de l’enseignement supérieur français est fallacieux. Des pays ayant mis en place ce type de barrière financière ont connu dans les années 
suivantes une baisse importante du nombre de leurs étudiant·e·s étranger·e·s. Cet argument camoufle mal la volonté d’éviction des jeunes chercheur·es, étudiants, et étudiantes internationales
 venant de pays en développement. Appliquer des frais d’inscription dix fois supérieurs aux étrangers extracommunautaires revient en effet à réserver l’accès à la formation, et à la 
recherche aux plus fortuné·e·s de ces pays, hormis quelques rares élu·e·s à qui sera attribué une bourse.  
Par ailleurs, **nous attirons l’attention sur le procédé sémantique devenu hégémonique qui consiste à employer des termes positifs pour nommer des procédures de sélection et d’exclusion.**
Dans ce langage « restriction des droits » devient « amélioration des conditions d’accueil » et la stratégie de fermeture des établissements français se voit renommée : « Bienvenue en 
France ». Ce glissement du langage loin d’être une simple stratégie de communication tend à réduire les capacités de penser négativement et donc de dénoncer les transformations actuelles 
à l’œuvre au sein de l’enseignement supérieur français.
 
En ce sens, le Collectif Marcel Mauss se positionne **contre toute proposition de règles différenciées ou de mesures particulières prises au regard de la nationalité des étudiant·e·s.**  
Il demande le retrait de cette mesure discriminatoire qui nuit au principe d’égal traitement des usagers du service public mais également à celui d’une Université française libre et ouverte
à tous.
