---
title: "Communiqué du 04-10-2018"
date: 2018-12-14T16:29:18+01:00
draft: false
---

### Communiqué du Collectif Marcel Mauss (04/10/2018)
Dans un contexte de privatisation de l'Université et face à la précarisation de nos conditions de travail
et de vie (baisse des APL, augmentation des loyers, augmentation des frais d'inscription…), nous défendons
le droit de mener nos études et nos recherches dans des conditions sereines. Nous sommes attachés à la
mission de service public qui est la nôtre, notamment dans le cadre de nos enseignements, ainsi qu'à la
production et du partage des savoirs.
Nous avons remarqué que des dynamiques locales s'organisaient sur la question des frais d'inscription
en doctorat mais il nous semble que seule une dynamique collective et nationale sur le sujet pourra infléchir
la situation. Nous résumons ci-dessous les points principaux.

##### Nos constats


 * En tant que personnel.le.s de l’Université, nous défendons nos droits de travailleu.se.r.s: faire de
la recherche un travail qui mérite rémunération, ce à quoi beaucoup d’entre nous n’ont
pourtant pas accès, malgré les heures passées à animer les activités de recherche au sein de leurs
laboratoires. Augmenter les frais d’inscription reviendrait donc à nous faire payer ce qui nous est
pourtant dû.


 * La situation de précarité des doctorant.e.s (augmentation des frais d'inscription, augmentation des loyers dans de
nombreuses villes françaises, thèses non financées, double temps de travail pour les contrats
CIFRE, inégalité des temps de thèse en fonction des disciplines, frais occasionnés par la
réalisation des missions de recherche…).


 * Manque de clarté et d'information sur les possibilités d'exonération des frais d'inscription et plus
largement sur les aides sociales disponibles. Sur ce point, les étudiant.es étranger.es ne sont pas
égaux devant la loi : au sein de l'Université de Bordeaux, lors de leur première année d'inscription,
ils ne sont pas éligibles à l'exonération des frais d'inscription. De plus, les étudiant.e.s étranger.es
détenteur.ice.s d'un titre de séjour "étudiant", cotisant pour toutes les contributions sociales n'ont
pas accès aux droits relatifs à ces cotisations.

 * Clarification du statut hybride : étudiant versus personnel de l’université.

##### Nos revendications


 * Suppression des frais d'inscription pour tous les doctorant.e.s, sans distinction aucune.

 * Sauvegarde et gratuité du service public.

 * Pas de discriminations entre les doctorats.

 * Clarification du statut : travailleur ? étudiant ?

##### Nos modes d'action possibles

 * Désengagement des activités scientifiques des laboratoires (organisation de séminaires,
investissement dans les axes…)

 * Désengagement des missions pédagogiques

 * Non-paiement des frais d'inscription (à ce jour, la très grande majorité des doctorant.e.s du
collectif Marcel Mauss n'ont pas payé leurs frais d'inscription).

 * Porter ces revendications et rallier toutes les mobilisations sociales à venir


Nous comptons sur vous pour diffuser localement et massivement ce premier communiqué synthétique
au sein de vos Universités et auprès de vos camardes, afin que nous soyons en nombre à nous engager dans
le mouvement.  
Aussi, nous vous invitons à rejoindre la mobilisation du 9 octobre 2018 et à porter nos
revendications lors cet évènement.

