---
title: "Soirée de Noel 2018"
date: 2018-12-14T16:29:18+01:00
draft: false
---

Nous sommes ravi·e·s de vous convier à la **première soirée de Noël**
organisée par le **Collectif Marcel Mauss**.

Issus de la volonté de rassembler plusieurs associations de jeunes
chercheur·e·s en sciences sociales (politistes, sociologues,
anthropologues…), le **Collectif Marcel Mauss** a été créé l’année dernière
avec notamment pour vocation de garantir une meilleur intégration et
cohésion au sein de nos laboratoires.

Cette soirée de Noël sera l’occasion de réunir doctorant·e·s et
professeur·e·s dans une ambiance conviviale et joviale afin de nous
connaître en dehors de l’enceinte de l’université tout en passant un moment
agréable.

Pendant la soirée, seront organisés des évènements tels qu’un quizz mettant
en compétition professeur·e·s et doctorant·e·s, ou encore l’élection de la
meilleure parodie des ouvrages issus écrits par les chercheur·e·s de nos
laboratoires.

Cette soirée se déroulera à la **Bodega Bodega le mardi 11 décembre** à
partir de 19h autour d’un menu tapas qui sera accompagné de boissons, tout
en laissant à chacun·e la possibilité d’augmenter les ressources de la
tablée !

Pour le menu nous avons pu proposer un prix, grâce au soutien des
laboratoires, de **10 euros** pour les doctorant·e·s et personnels
administratifs et de **20 euros** pour les professeur·e·s et maîtres de
conférences.

Les réservations se feront sur les deux sites où vous recevrez un ticket
valable pour la soirée. Soit **côté Victoire**, au bureau des doctorant·e·s
dans le département de sociologie, **de 10h à 13h**. Soit **côté IEP**, au
bureau A121 **du 3 au 10 décembre de 10h à 13h**.



**Voici le menu unique (avec possibilité d’assiettes végétariennes à
signaler lors de l’achat des tickets):**

**Tapas froides :**

Tortilla

Ardoises de jambon serrano

Cassolette de billes de tomates, olives, mozarella

Antipasti

**Tapas chaudes :**

Patatas bravas

Trio de croquetas

Calamar romana

Cassolette de seiches

Chistora

**Dessert :**

Carpaccio d'ananas frais

**Boissons comprises :**

1 pichet (sangria ou bière) de 1L pour 5 personnes

1 bouteille de vin pour 5 personnes



En espérant vous voir nombreuses et nombreux à cette soirée, n’attendez pas
le dernier moment pour réserver car les places sont limitées !
