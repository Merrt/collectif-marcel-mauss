---
title: "Home"
date: 2018-12-14T16:31:39+01:00
draft: false
---

Le [Collectif Marcel 
Mauss](https://merrt.frama.io/collectif-marcel-mauss/à-propos) a pour 
but **d'accompagner et de valoriser** les activités des jeunes 
chercheurs et chercheuses en sciences sociales du laboratoire [Centre 
Émile Durkheim](https://durkheim.u-bordeaux.fr/) de Bordeaux. Il se 
donne également pour objectif d'être un lieu de rencontre, d'information 
et d'échanges culturels et scientifiques, ainsi qu'un relais de leurs 
intérêts auprès des équipes administratives, scientifiques et pédagogiques
du laboratoire.

**Pour nous contacter:** collectifmarcelmauss@gmail.com


